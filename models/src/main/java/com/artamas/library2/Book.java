package com.artamas.library2;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
@AllArgsConstructor
public final class Book {
    @NotNull
    private Integer page_quantity;
    @NotNull
    private String name;
    @NotNull
    private Author author;
}
