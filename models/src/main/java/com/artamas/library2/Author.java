package com.artamas.library2;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
@AllArgsConstructor
public final class Author {
    @NotNull
    private String fio;
    @NotNull
    private String nationality;
}
