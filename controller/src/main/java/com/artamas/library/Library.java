package com.artamas.library;

import java.util.List;
import com.artamas.library2.Book;
import org.jetbrains.annotations.Nullable;

final class Library {
    @Nullable
    private List<Book> books;

    void setBooks(@Nullable List<Book> books) {
        this.books = books;
    }

    @Nullable
    List<Book> getBooks() {
        return books;
    }
}
