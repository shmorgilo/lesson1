package com.artamas.library;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

public final class Application {
    public static void main(@NotNull String[] args) {
        String name = String.valueOf(JOptionPane.showInputDialog("Enter writer name: "));
        LibraryFactory liba = new LibraryFactory();
        liba.createLibrary();
        Library filtred_lib = liba.searchByAuthor(name);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        System.out.println("Result:");
        System.out.println(gson.toJson(filtred_lib));
    }
}
