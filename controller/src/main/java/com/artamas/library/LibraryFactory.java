package com.artamas.library;

import com.artamas.library2.Author;
import com.artamas.library2.Book;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

final class LibraryFactory {
    @NotNull
    private static Library liba = new Library();

    LibraryFactory() {

    }

    void createLibrary() {
        try {
            List<Book> books = new ArrayList<>();
            Files.lines(Paths.get("text.txt")).forEach(g->{
                String[] split = g.split(",");
                Author author = new Author(split[1],split[0]);
                books.add(new Book(Integer.valueOf(split[3]),split[2],author));
            });
            liba = new Library();
            liba.setBooks(books);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @NotNull
    Library searchByAuthor(@NotNull String name) {
        List<Book> filtred_books = new ArrayList<>();
        assert liba.getBooks() != null;
        for (Book b : liba.getBooks()){
            if(b.getAuthor().getFio().equals(name)){
                filtred_books.add(b);
            }
        }
        Library filtred_liba = new Library();
        filtred_liba.setBooks(filtred_books);
        return filtred_liba;
    }
}
